---
permalink: /guides/copier-fichiers-cle-usb
---

# Copier des fichiers sur une clé USB ou un disque dur externe
[Revenir à l'accueil](/)

Les instructions sont identiques pour un disque dur externe. Je n'emploierai que le terme "clé USB" dans ce guide par simplicité.

## Matériel nécessaire 
* Un ordinateur
* Une clé USB

## Instructions
Une vidéo est disponible en-dessous. Vous pouvez la passer en plein écran pour mieux voir.

1. Brancher la clé USB
2. Ouvrir une fenêtre avec la clé USB :
    1. Si la fenêtre de l'Explorateur Windows n'apparaît pas automatiquement, ouvrir l'Explorateur Windows :
        * soit via le bouton jaune dans la barre des tâches (en bas)
        * soit en utilisant le raccourci clavier <kbd>Windows</kbd> + <kbd>E</kbd>. La touche <kbd>Windows</kbd> est celle avec le drapeau, à gauche ou à droite de la barre d'espace en bas du clavier.
    2. Dans la fenêtre qui apparaît, cliquer sur "Ce PC" dans le menu de gauche.
    3. Double cliquer ensuite sur le "disque" représentant la clé USB, généralement `D:` ou `E:`. Le nom est souvent en rapport avec la marque de la clé USB.
3. Déplacer la fenêtre sur la moitié droite de l'écran :
    1. Déplacer la souris jusqu'à la barre de titre en haut de la fenêtre.
    2. Cliquer et maintenir.
    3. Déplacer la souris jusqu'au bord droit de l'écran. Une légère animation doit s'afficher pour indiquer que la fenêter va prendre la moitié de l'écran.
    4. Relacher le clic.
4. Ouvrir une fenêtre avec les fichiers à copier :
    1. Ouvrir l'Explorateur Windows :
        * soit via le bouton jaune dans la barre des tâches (en bas)
        * soit en utilisant le raccourci clavier <kbd>Windows</kbd> + <kbd>E</kbd>. La touche <kbd>Windows</kbd> est celle avec le drapeau, à gauche ou à droite de la barre d'espace en bas du clavier.
    2. Naviguer jusqu'au dossier contenant les fichiers à copier.
        * Cliquer sur les dossiers dans le menu de gauche ou double-cliquer sur les dossiers dans la fenêtre principale.
5. Déplacer la nouvelle fenêtre sur la moitié gauche de l'écran :
    1. Déplacer la souris jusqu'à la barre de titre en haut de la fenêtre.
    2. Cliquer et maintenir.
    3. Déplacer la souris jusqu'au bord gauche de l'écran. Une légère animation doit s'afficher pour indiquer que la fenêter va prendre la moitié de l'écran.
    4. Relacher le clic.
6. Sélectionner les fichiers à copier :
    * Utiliser le clic gauche pour sélectionner un fichier
    * Maintenir la touche <kbd>Ctrl</kbd> enfoncée pour sélectionner plusieurs autres fichiers **non contigus**
    * Maintenir la touche <kbd>Maj</kbd> enfoncée pour sélectionner **contigus**
7. Clic droit sur un des fichiers précédemment sélectionnés
8. Cliquer sur "Copier"
9. Déplacer le curseur dans la fenêtre de droite (correspondant à la clé USB)
10. Clic droit dans la fenêtre
11. Cliquer sur "Coller"

En fonction du nombre de fichiers sélectionnés et de leur taille, le transfert peut être plus ou moins long.

Lorsque le transfert est fini, penser à [retirer en toute sécurité le périphérique USB](/guides/debrancher-usb)

## Vidéo

<iframe src="https://player.vimeo.com/video/373570869" width="100%" height="430" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
