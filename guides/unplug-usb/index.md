---
permalink: /guides/debrancher-usb
---

# Retirer en toute sécurité un périphérique de stockage USB
[Revenir à l'accueil](/)

Les instructions sont identiques pour un disque dur externe. Je n'emploierai que le terme "clé USB" dans ce guide par simplicité.

## Matériel nécessaire 
* Un ordinateur
* Une clé USB

## Instructions
1. Déplacer la souris en bas à droite de l'écran.
2. Cliquer sur la petite flèche qui pointe vers le haut.
3. Cliquer sur l'icône représentant un périphérique de stockage USB avec une coche verte.
4. Cliquer sur "Éjecter [clé usb]" ([clé usb] sera remplacé par le nom de votre périphérique)

Si un message d'erreur apparaît, fermer toutes les fenêtres et tous les programmes pouvant utiliser le périphérique (Explorateur Windows, aperçu des photos, etc.) et recommencer les étapes 1 à 4.

Vous pouvez maintenant débrancher le périphérique USB.
