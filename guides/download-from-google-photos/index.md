---
permalink: /guides/telecharger-google-photo
---

# Télécharger ses photos depuis Google Photos
[Revenir à l'accueil](/)

## Matériel nécessaire 
* Un ordinateur
* Une connexion à internet

## Instructions
Une vidéo est disponible en-dessous. Vous pouvez la passer en plein écran pour mieux voir.

1. Se rendre sur Google Photos ([https://photos.google.com](https://photos.google.com))
2. Sélectionner les photos à télécharger en cliquant sur la petite coche visible au survol de la souris sur une photo unique ou sur une journée entière
3. Cliquer sur les trois petits points en haut à droite
4. Cliquer sur "Télécharger"
5. Patienter, cela peut prendre du temps surtout si un grand nombre de photos est sélectionné
6. Profiter des photos sur votre ordinateur 😊

## Vidéo de l'interface de Google Photos

<iframe src="https://player.vimeo.com/video/373343691" width="100%" height="430" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
