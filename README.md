# Liste des guides

* [Télécharger ses photos depuis Google Photos](/guides/telecharger-google-photo)
* [Copier des fichiers sur un périphérique USB (clé ou disque dur)](/guides/copier-fichiers-cle-usb)
* [Retirer en toute sécurité un périphérique de stockage USB](/guides/debrancher-usb)

# Reuse
If you do reuse my work, please consider linking back to this repository 🙂